FROM openjdk:13

ADD build/libs/*.war /app.war

CMD java -Djava.security.egd=file:/dev/./urandom -jar /app.war

EXPOSE 8080
