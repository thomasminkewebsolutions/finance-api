package com.thomasminke.finance.api.summary.impl;

import com.thomasminke.finance.api.budget.Budget;
import com.thomasminke.finance.api.budget.BudgetService;
import com.thomasminke.finance.api.expense.ExpenseService;
import com.thomasminke.finance.api.expense.mapper.PeriodMapper;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.period.PeriodService;
import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.PostService;
import com.thomasminke.finance.api.post.mapper.PostMapper;
import com.thomasminke.finance.api.summary.SummaryService;
import com.thomasminke.finance.api.summary.dto.PeriodSummary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SummaryServiceImpl implements SummaryService {
    private final PostService postService;
    private final ExpenseService expenseService;
    private final PeriodService periodService;
    private final BudgetService budgetService;

    public SummaryServiceImpl(
            PostService postService,
            ExpenseService expenseService,
            PeriodService periodService,
            BudgetService budgetService
    ) {
        this.postService = postService;
        this.expenseService = expenseService;
        this.periodService = periodService;
        this.budgetService = budgetService;
    }

    @Override
    public PeriodSummary getPeriodSummary(Long periodId) {
        Period period = periodService.find(periodId);
        PeriodSummary periodSummary = new PeriodSummary(
                PeriodMapper.toDto(period)
        );

        List<Post> posts = postService.list();
        posts.forEach(post -> periodSummary.addPost(
                new PeriodSummary.PostPeriodSummary(
                        PostMapper.toDto(post),
                        expenseService.getSumOfPostPeriod(post, period),
                        budgetService.getAmountByPostAndPeriod(post, period))
                )
        );

        return periodSummary;
    }
}
