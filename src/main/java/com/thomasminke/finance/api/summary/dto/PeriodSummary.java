package com.thomasminke.finance.api.summary.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thomasminke.finance.api.period.dto.PeriodDTO;
import com.thomasminke.finance.api.post.dto.PostDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PeriodSummary {
    @JsonProperty("period")
    private PeriodDTO periodDTO;
    private List<PostPeriodSummary> posts = new ArrayList<>();

    public PeriodSummary(
            PeriodDTO periodDTO
    ) {
        this.periodDTO = periodDTO;
    }

    public List<PostPeriodSummary> getPosts() {
        return posts;
    }

    public void addPost(PostPeriodSummary postPeriodSummary) {
        posts.add(postPeriodSummary);
    }

    public static class PostPeriodSummary {
        @JsonProperty("post")
        private PostDTO postDTO;
        private BigDecimal sum;
        private BigDecimal budget;

        public PostPeriodSummary(
                PostDTO postDTO,
                BigDecimal sum,
                BigDecimal budget
        ) {
            this.postDTO = postDTO;
            this.sum = sum;
            this.budget = budget;
        }

        public PostDTO getPostDTO() {
            return postDTO;
        }

        public BigDecimal getSum() {
            return sum;
        }

        public BigDecimal getBudget() {
            return budget;
        }
    }

    public PeriodDTO getPeriodDTO() {
        return periodDTO;
    }
}
