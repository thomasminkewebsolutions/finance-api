package com.thomasminke.finance.api.summary;

import com.thomasminke.finance.api.summary.dto.PeriodSummary;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/summary")
public class SummaryController {
    private final SummaryService summaryService;

    public SummaryController(SummaryService summaryService) {
        this.summaryService = summaryService;
    }

    @GetMapping("/period/{id}")
    public ResponseEntity<PeriodSummary> getPeriodSummary(@PathVariable Long id) {
        return ResponseEntity.ok(summaryService.getPeriodSummary(id));
    }
}
