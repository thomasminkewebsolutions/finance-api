package com.thomasminke.finance.api.summary;

import com.thomasminke.finance.api.summary.dto.PeriodSummary;

public interface SummaryService {
    PeriodSummary getPeriodSummary(Long periodId);
}
