package com.thomasminke.finance.api.period;

import com.thomasminke.finance.api.period.dto.PeriodDTO;
import com.thomasminke.finance.api.period.request.CreatePeriodRequest;

import java.util.List;

public interface PeriodService {
    PeriodDTO create(CreatePeriodRequest createPeriodRequest);
    Period find(Long id);
    List<Period> list();
}
