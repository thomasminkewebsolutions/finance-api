package com.thomasminke.finance.api.period.request;

public class CreatePeriodRequest {
    private String name;

    private CreatePeriodRequest() {
    }

    public CreatePeriodRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
