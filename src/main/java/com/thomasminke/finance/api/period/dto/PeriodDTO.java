package com.thomasminke.finance.api.period.dto;

public class PeriodDTO {
    private Long id;
    private String name;

    public PeriodDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
