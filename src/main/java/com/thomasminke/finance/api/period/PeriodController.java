package com.thomasminke.finance.api.period;

import com.thomasminke.finance.api.expense.mapper.PeriodMapper;
import com.thomasminke.finance.api.period.dto.PeriodDTO;
import com.thomasminke.finance.api.period.request.CreatePeriodRequest;
import com.thomasminke.finance.api.post.dto.PostDTO;
import com.thomasminke.finance.api.post.mapper.PostMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/period")
public class PeriodController {
    private final PeriodService periodService;

    public PeriodController(PeriodService periodService) {
        this.periodService = periodService;
    }

    @PostMapping
    public ResponseEntity add(@RequestBody CreatePeriodRequest createPeriodRequest) {
        PeriodDTO periodDto = periodService.create(createPeriodRequest);
        return ResponseEntity
                .created(URI.create("/api/period/" + periodDto.getId()))
                .body(periodDto);
    }

    @GetMapping
    public ResponseEntity<List<PeriodDTO>> list() {
        return ResponseEntity.ok(
                periodService.list()
                        .stream()
                        .map(PeriodMapper::toDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PeriodDTO> find(@PathVariable Long id) {
        return ResponseEntity.ok(PeriodMapper.toDto(periodService.find(id)));
    }
}
