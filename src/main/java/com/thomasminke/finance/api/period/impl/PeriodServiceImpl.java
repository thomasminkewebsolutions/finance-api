package com.thomasminke.finance.api.period.impl;

import com.thomasminke.finance.api.expense.mapper.PeriodMapper;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.period.PeriodRepository;
import com.thomasminke.finance.api.period.PeriodService;
import com.thomasminke.finance.api.period.dto.PeriodDTO;
import com.thomasminke.finance.api.period.request.CreatePeriodRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeriodServiceImpl implements PeriodService {
    private final PeriodRepository repository;

    public PeriodServiceImpl(
            PeriodRepository repository
    ) {
        this.repository = repository;
    }

    @Override
    public PeriodDTO create(CreatePeriodRequest createPeriodRequest) {
        Period period = new Period(createPeriodRequest.getName());
        repository.saveAndFlush(period);
        return PeriodMapper.toDto(period);
    }

    @Override
    public Period find(Long id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public List<Period> list() {
        return repository.findAll();
    }
}
