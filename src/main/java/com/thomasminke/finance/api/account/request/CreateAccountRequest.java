package com.thomasminke.finance.api.account.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class CreateAccountRequest {
    private String name;

    @JsonCreator
    public CreateAccountRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
