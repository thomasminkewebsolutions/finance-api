package com.thomasminke.finance.api.account.impl;

import com.thomasminke.finance.api.account.Account;
import com.thomasminke.finance.api.account.AccountRepository;
import com.thomasminke.finance.api.account.AccountService;
import com.thomasminke.finance.api.account.dto.AccountDTO;
import com.thomasminke.finance.api.account.mapper.AccountMapper;
import com.thomasminke.finance.api.account.request.CreateAccountRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository repository;

    public AccountServiceImpl(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public AccountDTO create(CreateAccountRequest createAccountRequest) {
        Account account = new Account(createAccountRequest.getName());
        repository.saveAndFlush(account);
        return AccountMapper.toDto(account);
    }

    @Override
    public Account find(Long id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public List<Account> list() {
        return repository.findAll();
    }

    @Override
    public void subtractFromBalance(Account account, BigDecimal amount) {
        account.subtractFromBalance(amount);
        repository.saveAndFlush(account);
    }
}
