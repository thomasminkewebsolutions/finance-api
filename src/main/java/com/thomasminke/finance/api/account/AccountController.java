package com.thomasminke.finance.api.account;

import com.thomasminke.finance.api.account.dto.AccountDTO;
import com.thomasminke.finance.api.account.mapper.AccountMapper;
import com.thomasminke.finance.api.account.request.CreateAccountRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/account")
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<AccountDTO> create(@RequestBody CreateAccountRequest createAccountRequest) {
        AccountDTO accountDTO = accountService.create(createAccountRequest);
        return ResponseEntity
                .created(URI.create("/api/account/" + accountDTO.getId()))
                .body(accountDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountDTO> find(@PathVariable Long id) {
        return ResponseEntity.ok(AccountMapper.toDto(accountService.find(id)));
    }

    @GetMapping
    public ResponseEntity<List<AccountDTO>> list() {
        return ResponseEntity.ok(
                accountService.list()
                .stream()
                .map(AccountMapper::toDto)
                .collect(Collectors.toList())
        );
    }
}
