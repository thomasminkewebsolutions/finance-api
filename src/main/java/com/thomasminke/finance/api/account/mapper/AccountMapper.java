package com.thomasminke.finance.api.account.mapper;

import com.thomasminke.finance.api.account.Account;
import com.thomasminke.finance.api.account.dto.AccountDTO;

public class AccountMapper {
    public static AccountDTO toDto(Account account) {
        return new AccountDTO(
                account.getId(),
                account.getName(),
                account.getBalance()
        );
    }
}
