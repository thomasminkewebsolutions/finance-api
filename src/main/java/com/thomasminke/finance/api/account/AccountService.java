package com.thomasminke.finance.api.account;

import com.thomasminke.finance.api.account.dto.AccountDTO;
import com.thomasminke.finance.api.account.request.CreateAccountRequest;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {
    AccountDTO create(CreateAccountRequest createAccountRequest);
    Account find(Long id);
    List<Account> list();
    void subtractFromBalance(Account account, BigDecimal amount);
}
