package com.thomasminke.finance.api.budget.dto;

import java.math.BigDecimal;

public class BudgetDTO {
    private Long id;
    private Long postId;
    private Long periodId;
    private BigDecimal amount;

    public BudgetDTO(
            Long id,
            Long postId,
            Long periodId,
            BigDecimal amount
    ) {
        this.id = id;
        this.postId = postId;
        this.periodId = periodId;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public Long getPostId() {
        return postId;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
