package com.thomasminke.finance.api.budget.mapper;

import com.thomasminke.finance.api.budget.Budget;
import com.thomasminke.finance.api.budget.dto.BudgetDTO;

public class BudgetMapper {
    public static BudgetDTO toDto(Budget budget) {
        return new BudgetDTO(
                budget.getId(),
                budget.getPost().getId(),
                budget.getPeriod().getId(),
                budget.getAmount()
        );
    }
}
