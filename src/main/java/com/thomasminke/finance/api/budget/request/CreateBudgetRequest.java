package com.thomasminke.finance.api.budget.request;

import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import java.math.BigDecimal;

public class CreateBudgetRequest {
    private Post post;
    private Period period;
    private BigDecimal amount;

    public CreateBudgetRequest(
            Post post,
            Period period,
            BigDecimal amount
    ) {
        this.post = post;
        this.period = period;
        this.amount = amount;
    }

    public Post getPost() {
        return post;
    }

    public Period getPeriod() {
        return period;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
