package com.thomasminke.finance.api.budget;

import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BudgetRepository extends JpaRepository<Budget, Long> {
    Optional<Budget> findByPostAndPeriod(Post post, Period period);
}
