package com.thomasminke.finance.api.budget;

import com.thomasminke.finance.api.budget.dto.BudgetDTO;
import com.thomasminke.finance.api.budget.request.CreateBudgetRequest;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import java.math.BigDecimal;

public interface BudgetService {
    BudgetDTO createBudget(CreateBudgetRequest request);
    BigDecimal getAmountByPostAndPeriod(Post post, Period period);
    Budget getBudgetByPostAndPeriod(Post post, Period period);
}
