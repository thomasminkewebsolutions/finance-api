package com.thomasminke.finance.api.budget;

import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "budget")
public class Budget {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Post post;

    @ManyToOne
    private Period period;

    private BigDecimal amount;

    private Budget() {
    }

    public Budget(
            Post post,
            Period period,
            BigDecimal amount
    ) {
        this.post = post;
        this.period = period;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public Post getPost() {
        return post;
    }

    public Period getPeriod() {
        return period;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
