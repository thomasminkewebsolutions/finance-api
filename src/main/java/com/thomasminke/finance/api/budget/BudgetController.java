package com.thomasminke.finance.api.budget;

import com.thomasminke.finance.api.budget.dto.BudgetDTO;
import com.thomasminke.finance.api.budget.dto.CreateBudgetRequestDTO;
import com.thomasminke.finance.api.budget.request.CreateBudgetRequest;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.period.PeriodService;
import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.PostService;
import com.thomasminke.finance.api.post.dto.PostWithBudgetDTO;
import com.thomasminke.finance.api.post.mapper.PostMapper;
import com.thomasminke.finance.api.post.mapper.PostWithBudgetMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/budget")
public class BudgetController {
    private final BudgetService budgetService;
    private final PostService postService;
    private final PeriodService periodService;

    public BudgetController(BudgetService budgetService, PostService postService, PeriodService periodService) {
        this.budgetService = budgetService;
        this.postService = postService;
        this.periodService = periodService;
    }

    @PostMapping
    public ResponseEntity addBudget(@RequestBody CreateBudgetRequestDTO requestDTO) {
        CreateBudgetRequest createBudgetRequest = new CreateBudgetRequest(
                postService.find(requestDTO.getPostId()),
                periodService.find(requestDTO.getPeriodId()),
                requestDTO.getAmount()
        );

        BudgetDTO budgetDTO = budgetService.createBudget(createBudgetRequest);
        return ResponseEntity
                .created(URI.create("/api/budget/" + budgetDTO.getId()))
                .body(budgetDTO);
    }

    @GetMapping("/period/{periodId}")
    public List<PostWithBudgetDTO> getFromPeriod(@PathVariable Long periodId) {
        Period period = periodService.find(periodId);
        List<Post> posts = postService.list();
        return posts
                .stream()
                .map(post -> PostWithBudgetMapper.toDto(post, budgetService.getBudgetByPostAndPeriod(post, period)))
                .collect(Collectors.toList()
                );
    }
}
