package com.thomasminke.finance.api.budget.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.math.BigDecimal;

public class CreateBudgetRequestDTO {
    private Long postId;
    private Long periodId;
    private BigDecimal amount;

    @JsonCreator
    public CreateBudgetRequestDTO(
            Long postId,
            Long periodId,
            BigDecimal amount
    ) {
        this.postId = postId;
        this.periodId = periodId;
        this.amount = amount;
    }

    public Long getPostId() {
        return postId;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
