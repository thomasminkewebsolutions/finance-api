package com.thomasminke.finance.api.budget.impl;

import com.thomasminke.finance.api.budget.Budget;
import com.thomasminke.finance.api.budget.BudgetRepository;
import com.thomasminke.finance.api.budget.BudgetService;
import com.thomasminke.finance.api.budget.dto.BudgetDTO;
import com.thomasminke.finance.api.budget.mapper.BudgetMapper;
import com.thomasminke.finance.api.budget.request.CreateBudgetRequest;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class BudgetServiceImpl implements BudgetService {
    private final BudgetRepository repository;

    public BudgetServiceImpl(BudgetRepository repository) {
        this.repository = repository;
    }

    @Override
    public BudgetDTO createBudget(CreateBudgetRequest request) {
        Budget budget = new Budget(
                request.getPost(),
                request.getPeriod(),
                request.getAmount()
        );
        repository.save(budget);

        return BudgetMapper.toDto(budget);
    }

    @Override
    public BigDecimal getAmountByPostAndPeriod(Post post, Period period) {
        Optional<Budget> optionalBudget = repository.findByPostAndPeriod(post, period);
        return optionalBudget.isPresent() ? optionalBudget.get().getAmount() : new BigDecimal("0.0");
    }

    @Override
    public Budget getBudgetByPostAndPeriod(Post post, Period period) {
        Optional<Budget> optionalBudget = repository.findByPostAndPeriod(post, period);
        return optionalBudget.isPresent() ? optionalBudget.get() : null;
    }
}
