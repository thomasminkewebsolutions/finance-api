package com.thomasminke.finance.api.post.dto;

public class PostDTO {
    private Long id;
    private String name;

    private PostDTO() {
    }

    public PostDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
