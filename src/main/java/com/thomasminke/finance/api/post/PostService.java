package com.thomasminke.finance.api.post;

import com.thomasminke.finance.api.post.dto.PostDTO;
import com.thomasminke.finance.api.post.request.CreatePostRequest;

import java.util.List;

public interface PostService {
    PostDTO create(CreatePostRequest createPostRequest);
    List<Post> list();
    Post find(Long id);
    void delete(Long id);
}
