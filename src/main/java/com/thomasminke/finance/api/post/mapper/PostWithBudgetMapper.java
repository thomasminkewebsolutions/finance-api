package com.thomasminke.finance.api.post.mapper;

import com.thomasminke.finance.api.budget.Budget;
import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.dto.PostWithBudgetDTO;

import java.math.BigDecimal;

public class PostWithBudgetMapper {
    public static PostWithBudgetDTO toDto(Post post, Budget budget) {
        return new PostWithBudgetDTO(
                post.getId(),
                post.getName(),
                budget != null ? budget.getAmount() : null
        );
    }
}
