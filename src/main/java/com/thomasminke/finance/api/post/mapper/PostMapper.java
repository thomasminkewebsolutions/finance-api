package com.thomasminke.finance.api.post.mapper;

import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.dto.PostDTO;

public class PostMapper {
    public static PostDTO toDto(Post post) {
        return new PostDTO(
                post.getId(),
                post.getName()
        );
    }

    public static Post toEntity(PostDTO postDTO) {
        return new Post(
                postDTO.getName()
        );
    }
}
