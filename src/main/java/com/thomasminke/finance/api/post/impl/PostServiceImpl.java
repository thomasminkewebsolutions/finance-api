package com.thomasminke.finance.api.post.impl;

import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.PostRepository;
import com.thomasminke.finance.api.post.PostService;
import com.thomasminke.finance.api.post.dto.PostDTO;
import com.thomasminke.finance.api.post.mapper.PostMapper;
import com.thomasminke.finance.api.post.request.CreatePostRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository repository;

    public PostServiceImpl(PostRepository repository) {
        this.repository = repository;
    }

    @Override
    public PostDTO create(CreatePostRequest createPostRequest) {
        Post post = new Post(createPostRequest.getName());
        repository.saveAndFlush(post);
        return PostMapper.toDto(post);
    }

    @Override
    public List<Post> list() {
        return repository
                .findAll();
    }

    @Override
    public Post find(Long id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
