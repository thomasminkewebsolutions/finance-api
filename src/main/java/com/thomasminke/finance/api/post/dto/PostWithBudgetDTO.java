package com.thomasminke.finance.api.post.dto;

import java.math.BigDecimal;

public class PostWithBudgetDTO {
    private Long id;
    private String name;
    private BigDecimal budget;

    public PostWithBudgetDTO(Long id, String name, BigDecimal budget) {
        this.id = id;
        this.name = name;
        this.budget = budget;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBudget() {
        return budget;
    }
}
