package com.thomasminke.finance.api.post.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class CreatePostRequest {
    private String name;

    @JsonCreator
    public CreatePostRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
