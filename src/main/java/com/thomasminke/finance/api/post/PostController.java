package com.thomasminke.finance.api.post;

import com.thomasminke.finance.api.post.dto.PostDTO;
import com.thomasminke.finance.api.post.impl.PostServiceImpl;
import com.thomasminke.finance.api.post.mapper.PostMapper;
import com.thomasminke.finance.api.post.request.CreatePostRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/post")
public class PostController {
    private final PostServiceImpl postService;

    public PostController(PostServiceImpl postService) {
        this.postService = postService;
    }

    @PostMapping
    public ResponseEntity<PostDTO> create(@RequestBody CreatePostRequest createPostRequest) {
        PostDTO postDTO = postService.create(createPostRequest);
        return ResponseEntity
                .created(URI.create("/api/post/" + postDTO.getId()))
                .body(postDTO);
    }

    @GetMapping
    public ResponseEntity<List<PostDTO>> list() {
        return ResponseEntity.ok(
                postService.list()
                        .stream()
                        .map(PostMapper::toDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PostDTO> find(@PathVariable Long id) {
        return ResponseEntity.ok(PostMapper.toDto(postService.find(id)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        postService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
