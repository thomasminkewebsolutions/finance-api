package com.thomasminke.finance.api.expense;

import com.thomasminke.finance.api.account.Account;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "expense")
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;
    private LocalDate date;
    private BigDecimal amount;

    @ManyToOne
    private Account account;

    @ManyToOne
    private Period period;

    @ManyToOne
    private Post post;

    private Expense() {
    }

    public Expense(
            String description,
            LocalDate date,
            BigDecimal amount,
            Account account,
            Period period,
            Post post
    ) {
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.account = account;
        this.period = period;
        this.post = post;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Account getAccount() {
        return account;
    }

    public Period getPeriod() {
        return period;
    }

    public Post getPost() {
        return post;
    }
}
