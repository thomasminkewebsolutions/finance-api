package com.thomasminke.finance.api.expense;

import com.thomasminke.finance.api.expense.dto.ExpenseDTO;
public class ExpenseMapper {
    public static ExpenseDTO toDto(Expense expense) {
        return new ExpenseDTO(
            expense.getId(),
            expense.getDescription(),
            expense.getDate(),
            expense.getAmount(),
            expense.getAccount().getId()
        );
    }
}
