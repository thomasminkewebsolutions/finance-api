package com.thomasminke.finance.api.expense.mapper;

import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.period.dto.PeriodDTO;

public class PeriodMapper {
    public static PeriodDTO toDto(Period period) {
        return new PeriodDTO(period.getId(), period.getName());
    }
}
