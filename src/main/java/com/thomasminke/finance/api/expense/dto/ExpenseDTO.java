package com.thomasminke.finance.api.expense.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExpenseDTO {
    private Long id;
    private String description;
    private LocalDate date;
    private BigDecimal amount;
    private Long accountId;

    public ExpenseDTO(Long id, String description, LocalDate date, BigDecimal amount, Long accountId) {
        this.id = id;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.accountId = accountId;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Long getAccountId() {
        return accountId;
    }
}
