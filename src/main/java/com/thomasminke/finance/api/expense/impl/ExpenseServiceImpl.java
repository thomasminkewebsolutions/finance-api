package com.thomasminke.finance.api.expense.impl;

import com.thomasminke.finance.api.account.AccountService;
import com.thomasminke.finance.api.budget.Budget;
import com.thomasminke.finance.api.budget.BudgetService;
import com.thomasminke.finance.api.expense.Expense;
import com.thomasminke.finance.api.expense.ExpenseMapper;
import com.thomasminke.finance.api.expense.ExpenseRepository;
import com.thomasminke.finance.api.expense.ExpenseService;
import com.thomasminke.finance.api.expense.dto.ExpenseDTO;
import com.thomasminke.finance.api.expense.request.AddExpenseRequest;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
public class ExpenseServiceImpl implements ExpenseService {
    private final AccountService accountService;
    private final ExpenseRepository repository;
    private final BudgetService budgetService;

    public ExpenseServiceImpl(
            AccountService accountService,
            ExpenseRepository repository,
            BudgetService budgetService) {
        this.accountService = accountService;
        this.repository = repository;
        this.budgetService = budgetService;
    }

    @Override
    @Transactional
    public ExpenseDTO add(AddExpenseRequest addExpenseRequest) {
        Budget budget = budgetService.getBudgetByPostAndPeriod(
                addExpenseRequest.getPost(),
                addExpenseRequest.getPeriod()
        );
        if(budget == null) {
            throw new IllegalStateException("No budget available for this period and post.");
        }

        Expense expense = new Expense(
                addExpenseRequest.getDescription(),
                addExpenseRequest.getDate(),
                addExpenseRequest.getAmount(),
                addExpenseRequest.getAccount(),
                addExpenseRequest.getPeriod(),
                addExpenseRequest.getPost()
        );

        repository.save(expense);
        accountService.subtractFromBalance(
                addExpenseRequest.getAccount(),
                addExpenseRequest.getAmount()
        );

        return ExpenseMapper.toDto(expense);
    }

    @Override
    public BigDecimal getSumOfPostPeriod(Post post, Period period) {
        BigDecimal sum = repository.sumPostPeriod(post.getId(), period.getId());
        return sum != null ? sum : new BigDecimal("0.0");
    }

    @Override
    public List<Expense> findByPost(Post post) {
        return repository.findByPost(post);
    }
}
