package com.thomasminke.finance.api.expense.request;

import com.thomasminke.finance.api.account.Account;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import java.math.BigDecimal;
import java.time.LocalDate;

public class AddExpenseRequest {
    private String description;
    private LocalDate date;
    private BigDecimal amount;
    private Account account;
    private Period period;
    private Post post;

    public AddExpenseRequest(
            String description,
            LocalDate date,
            BigDecimal amount,
            Account account,
            Period period,
            Post post
    ) {
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.account = account;
        this.period = period;
        this.post = post;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Account getAccount() {
        return account;
    }

    public Period getPeriod() {
        return period;
    }

    public Post getPost() {
        return post;
    }
}
