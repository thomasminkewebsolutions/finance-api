package com.thomasminke.finance.api.expense;

import com.thomasminke.finance.api.expense.dto.ExpenseDTO;
import com.thomasminke.finance.api.expense.request.AddExpenseRequest;
import com.thomasminke.finance.api.period.Period;
import com.thomasminke.finance.api.post.Post;

import java.math.BigDecimal;
import java.util.List;

public interface ExpenseService {
    ExpenseDTO add(AddExpenseRequest addExpenseRequest);
    BigDecimal getSumOfPostPeriod(Post post, Period period);
    List<Expense> findByPost(Post post);
}
