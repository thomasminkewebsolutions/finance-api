package com.thomasminke.finance.api.expense;

import com.thomasminke.finance.api.account.AccountService;
import com.thomasminke.finance.api.account.mapper.AccountMapper;
import com.thomasminke.finance.api.expense.dto.ExpenseDTO;
import com.thomasminke.finance.api.expense.dto.AddExpenseRequestDTO;
import com.thomasminke.finance.api.expense.request.AddExpenseRequest;
import com.thomasminke.finance.api.period.PeriodService;
import com.thomasminke.finance.api.post.Post;
import com.thomasminke.finance.api.post.PostService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/expense")
public class ExpenseController {
    private final ExpenseService expenseService;
    private final AccountService accountService;
    private final PeriodService periodService;
    private final PostService postService;

    public ExpenseController(
            ExpenseService expenseService,
            AccountService accountService,
            PeriodService periodService,
            PostService postService
    ) {
        this.expenseService = expenseService;
        this.accountService = accountService;
        this.periodService = periodService;
        this.postService = postService;
    }

    @GetMapping("/post/{postId}")
    public ResponseEntity<List<ExpenseDTO>> getFromPost(@PathVariable Long postId) {
        Post post = postService.find(postId);
        List<Expense> expenseList = expenseService.findByPost(post);

        return ResponseEntity.ok(expenseList.stream()
                .map(ExpenseMapper::toDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<ExpenseDTO> add(@RequestBody AddExpenseRequestDTO addExpenseRequestDTO) {
        AddExpenseRequest addExpenseRequest = new AddExpenseRequest(
                addExpenseRequestDTO.getDescription(),
                addExpenseRequestDTO.getDate(),
                addExpenseRequestDTO.getAmount(),
                accountService.find(addExpenseRequestDTO.getAccountId()),
                periodService.find(addExpenseRequestDTO.getPeriodId()),
                postService.find(addExpenseRequestDTO.getPostId())
        );
        ExpenseDTO expenseDTO = expenseService.add(addExpenseRequest);
        return ResponseEntity
                .created(URI.create("/api/expense/" + expenseDTO.getId()))
                .body(expenseDTO);
    }
}
