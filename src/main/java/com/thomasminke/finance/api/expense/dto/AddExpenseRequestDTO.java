package com.thomasminke.finance.api.expense.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.math.BigDecimal;
import java.time.LocalDate;

public class AddExpenseRequestDTO {
    private String description;
    private LocalDate date;
    private BigDecimal amount;
    private Long accountId;
    private Long periodId;
    private Long postId;

    @JsonCreator
    public AddExpenseRequestDTO(
            String description,
            LocalDate date,
            BigDecimal amount,
            Long accountId,
            Long periodId,
            Long postId
    ) {
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.accountId = accountId;
        this.periodId = periodId;
        this.postId = postId;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public Long getPeriodId() {
        return periodId;
    }

    public Long getPostId() {
        return postId;
    }
}
