package com.thomasminke.finance.api.expense;

import com.thomasminke.finance.api.post.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    @Query(nativeQuery = true, value = "" +
            "SELECT sum(amount) " +
            "FROM expense e " +
            "WHERE e.period_id = :periodId " +
            "AND e.post_id = :postId")
    BigDecimal sumPostPeriod(@Param("postId") Long postId, @Param("periodId") Long periodId);

    List<Expense> findByPost(Post post);
}
